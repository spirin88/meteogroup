module.exports = function(grunt) {

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var path = grunt.option('path');

    grunt.log.writeln([path]);

    grunt.log.writeln('Waiting...');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            js_core: {
                files: {
                    '../js/core.min.js': [
                        './js/jquery/jquery.min.js',
                        './js/foundation/foundation.core.js',
                        './js/foundation/foundation.util.mediaQuery.js',
                        './js/apps/app.core.js',
                        './js/apps/*.js'
                    ]
                },
                options: {
                    mangle: false,
                    sourceMap: true,
                    preserveComments: false
                }
            },
        },
        jshint: {
            js_core: [
                './js/apps/*.js'
            ],
        },
        compass: {
            dist: {
                options: {
                    //basePath: '<%= path %>',
                    sassDir: './sass',
                    httpPath: '/',
                    httpStylesheetsPath: 'css',
                    cssDir: '../css',
                    cssPath: '../css',
                    relativeAssets: true,
                    boring: false,
                    debugInfo: true,
                    environment: 'development',
                    outputStyle: 'compressed',
                    sourcemap: true
                }
            }
        },
        watch: {
            sass: {
                files: [
                    './sass/*.scss',
                    './sass/**/*.scss',
                    './sass/**/**/*.scss',
                    './sass/**/**/**/*.scss'
                ],
                tasks: ['compass']
            },
            js_core: {
                files: [
                    './js/apps/**/*.js'
                ],
                tasks: ['jshint:js_core', 'uglify:js_core']
            },
            options: { livereload: false },
        }
    });

    grunt.registerTask('default',['watch']);
    grunt.registerTask('compile_dev', ['compass', 'jshint', 'uglify']);
}