;(function ($, window, document, undefined) {
	'use strict';

	$.meteoGroup = $.meteoGroup || {};

	var url = './assets/src/forecast.json',
		datePlaceholder = $('.js-date'),
		windPowerPlaceholder = $('.js-wind-power'),
		windPowerUtilization = $('.js-wind-power-utilization'),
		windPowerCapacity = $('.js-wind-power-capacity'),
		prevDatePlaceholder = $('.js-prev-date'),
		prevWindPowerPlaceholder = $('.js-prev-wind-power'),
		prevWindPowerUtilization = $('.js-prev-wind-power-utilization'),
		prevWindPowerCapacity = $('.js-prev-wind-power-capacity'),
		nextDatePlaceholder = $('.js-next-date'),
		nextWindPowerPlaceholder = $('.js-next-wind-power'),
		nextWindPowerUtilization = $('.js-next-wind-power-utilization'),
		nextWindPowerCapacity = $('.js-next-wind-power-capacity'),
		setCurrentTime = $('.js-current-time');

	$.meteoGroup = {
		getData : function () {


			$.getJSON(url, function(json) {

				var forecast = json.data,
					currentTime = new Date(), // IN REAL WORLD IT WOULD BE SERVER TIME
					time = currentTime.getHours();

				setCurrentTime.html(currentTime);

				$.each(forecast, function (i, report) {

					if (i == time) {
						datePlaceholder.html(report.dateTime);
						windPowerPlaceholder.html(report.windPower);
						windPowerUtilization.html(report.windPowerUtilization);
						windPowerCapacity.html(report.windPowerCapacity);
					}

					if (i == time - 1) {
						prevDatePlaceholder.html(report.dateTime);
						prevWindPowerPlaceholder.html(report.windPower);
						prevWindPowerUtilization.html(report.windPowerUtilization);
						prevWindPowerCapacity.html(report.windPowerCapacity);
					}

					if (i == time + 1) {
						nextDatePlaceholder.html(report.dateTime);
						nextWindPowerPlaceholder.html(report.windPower);
						nextWindPowerUtilization.html(report.windPowerUtilization);
						nextWindPowerCapacity.html(report.windPowerCapacity);
					}
				});
			});
		},
		init : function() {

			$(document).foundation();

			$.meteoGroup.getData();

			// GETTING UPDATED DATA
			setInterval(function () { $.meteoGroup.getData(); }, 60*1000);
		}
	};

	$(function () {
		$.meteoGroup.init(); 
	});

}(jQuery, window, window.document));